<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MK Music Studio</title>
    <meta name="description" content="Your Description Here">
    <meta name="keywords" content="bootstrap themes, portfolio, responsive theme">
    <meta name="author" content="ThemeForces.Com">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="tf-home">
        <div class="overlay">
            <div id="sticky-anchor"></div>
            <nav id="tf-menu" class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <!-- <a class="navbar-brand logo" href="index.html"><img src="img/mk coret.jpg" width="40" align="center"></a> -->
                      <ul class="nav navbar-nav navbar-left">
                        <li><a href="#"><span class="fa fa-facebook" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-soundcloud" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-youtube" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-instagram" style="font-size: 20px;"></span></a></li>
                      </ul>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">Kembali Ke Halaman Utama</a></li>
                        <!-- <li><a href="#tf-why-me">Studio</a></li>
                        <li><a href="#tf-about">Rekaman</a></li>
                        <li><a href="#tf-portfolio">Alat</a></li>
                        <li><a href="#tf-contact">Tentang Kami</a></li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="container" style="min-height: 560px;">
                <div class="row" align="center">
                    <h1>DAFTAR MEMBER BARU</h1>
                    <h2>Silahkan isi form dibawah untuk menjadi member</h2>
                    <br>
                    <div style="border: 0; padding: 20px; background: none; width: 600px; text-align: center; color: #FFFFFF;">
                        <form method="post" action="c_user.php?aksi=add">
                        <table width=100% cellspacing=40>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <h3 align="left">Nama</h3>
                                        <input type="text" class="form-control" placeholder="Nama" name="username">
                                    </div>
                                    <div class="form-group">
                                        <h3 align="left">Password</h3>
                                        <input type="password" class="form-control" placeholder="Password" name="password">
                                    </div>
                                </td>
                                <td width=10%> </td>
                                <td>
                                    <div class="form-group">
                                        <h3 align="left">E-Mail</h3>
                                        <input type="text" class="form-control" placeholder="E-Mail" name="email">
                                    </div>
                                    <div class="form-group">
                                        <h3 align="left">Nomer Handphone</h3>
                                        <input type="text" class="form-control" placeholder="Nomer Handphone" name="hp">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        
                        <div>
                            <table rules="rows">
                                <tr>
                                    <td style="width: 100px; text-align: center;"><input type="submit" name='submit' class="btn btn-primary" value="Simpan" ></td>
                                    <td style="width: 10px"></td>
                                    <td style="width: 100px; text-align: center;"><a href="login.php" class="btn btn-danger">Batal</a></td>
                                </tr>
                            </table> 
                        </form>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>