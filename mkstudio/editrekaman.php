<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MK Music Studio</title>
    <meta name="description" content="Your Description Here">
    <meta name="keywords" content="bootstrap themes, portfolio, responsive theme">
    <meta name="author" content="ThemeForces.Com">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="tf-home">
        <div style="background: #222222;">
            <nav id="tf-menu" class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header" style="height: 10px;">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <ul class="nav navbar-nav navbar-left">
                        <li><a href="#"><span class="fa fa-envelope" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-facebook" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-instagram" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-soundcloud" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-youtube" style="font-size: 20px;"></span></a></li>
                      </ul>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">MK Music Studio</a></li>
                      </ul>
                    </div>
                </div>
            </nav>

        </div>
    </div>

    <div id="tf-contact">
        <div class="container" style="min-height: 330px;">
            <div class="section-title" style="color: #FFFFFF; text-align: center;">
                <h2>Booking Studio Rekaman</h2>
            </div>

            <div class="space"></div>

            <div class="row" align="center">
                <table>
                    <tr>
                        <td style="width: 20%; vertical-align: top; background: url(img/transparan.png); color: #FFFFFF; padding: 10px">
                            <p>Isi semua form. Pilih jenis paket sesuai pilihan anda. Sertakan bukti Transfer. Jika sudah terisi semua klik tombol "Simpan".</p>
                            <p>Daftar harga layanan kami :</p>
                            <p>1. Paket 1 Lagu Rp. 650.000,-</p>
                            <p>2. Recording Rp. 250.000,-</p>
                            <p>3. Mixing Rp. 250.000,-</p>
                            <p>4. Mastering Rp. 250.000,-</p>
                            <p>NB : DP minimal 50% dari harga total.</p>
                        </td>
                        <td style="width: 50px"></td>
                        <td style="vertical-align: middle;">
                            <table class="table" style="background: url(img/transparan.png);">
                                <form method="post" action="c_rekaman.php?aksi=edit">
                                <input value="<?php echo $data['id_rec'];?>" type="hidden" name="id_rec" class="form-control">
                                <tr> <div class="form-group"><td width="15%" style="vertical-align: top; color: #FFFFFF;">Nama Band</td> <td width="3%" style="vertical-align: top; color: #FFFFFF;">:</td> <td> <input style="width: 100%" type="text" name="nama_rec" class="form-group" placeholder="Nama Band" value="<?php echo $data['nama_rec'];?>" required></td></div> </tr>

                                <tr> <td width="15%" style="vertical-align: top; color: #FFFFFF;">Tanggal</td> <td width="3%" style="vertical-align: top; color: #FFFFFF;">:</td> <td> <input style="width: 100%" type="datepicker" name="tanggal_rec" class="form-group" placeholder="Tanggal (ex. YYYY-MM-DD)" value="<?php echo $data['tanggal_rec'];?>" required></td> </tr>

                                <tr> <td width="15%" style="vertical-align: top; color: #FFFFFF;">Jenis Layanan</td> <td width="3%"style="vertical-align: top; color: #FFFFFF;">:</td> <td><input style="width: 100%" type="text" name="jenis_rec" class="form-group" placeholder="Jenis Layanan" value="<?php echo $data['jenis_rec'];?>"required>
                                </td> </tr>

                                <tr> <td width="15%" style="vertical-align: top; color: #FFFFFF;">Bukti Transfer</td> <td width="3%" style="vertical-align: top; color: #FFFFFF;">:</td> <td> <input style="width: 100%" type="text" name="bukti_rec" class="form-group" placeholder="Bukti Transfer" value="<?php echo $data['bukti_rec'];?>" required></td> </tr>

                                <tr> <td width="15%" style="vertical-align: top; color: #FFFFFF;">Status</td> <td width="3%" style="vertical-align: top; color: #FFFFFF;">:</td> <td> <select style="width: 100%" type="text" name="status_rec" class="form-group" placeholder="Status Rekaman" value="<?php echo $data['status_rec'];?>" required> 
                                <option value="">On Book</option>  
                                <option value="Apel">Done</option></select>
                                </td> </tr>

                                
                                <tr>
                                    <table>
                                        <tr>
                                            <td><input type="submit" name='submit' class="btn btn-primary my-btn" value="Simpan" ></td>
                                            <td><a href="rekaman.php" type="button" class="btn btn-danger">Kembali</a></td>
                                        </tr>
                                    </table>
                                </tr>
                                </form>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
    </div>

    <nav id="tf-footer">
        <div class="container">
             <div class="pull-left">
                <p>2017 © Official Website MK Music Studio. All Rights Reserved. Designed and Coded by MK Music Management for <a href="https://themeforces.com">MK Music Studio</a></p>
            </div>
        </div>
    </nav>
   

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>


  </body>
</html>