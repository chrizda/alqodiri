<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MK Music Studio</title>
    <meta name="description" content="Your Description Here">
    <meta name="keywords" content="bootstrap themes, portfolio, responsive theme">
    <meta name="author" content="ThemeForces.Com">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="tf-home">
        <div class="overlay">
            <div id="sticky-anchor"></div>
            <nav id="tf-menu" class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      
                      <ul class="nav navbar-nav navbar-left">
                        <li><a href="#"><span class="fa fa-envelope" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-facebook" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-instagram" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-soundcloud" style="font-size: 20px;"></span></a></li>
                        <li><a href="#"><span class="fa fa-youtube" style="font-size: 20px;"></span></a></li>
                      </ul>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#tf-home">Home</a></li>
                        <li><a href="#tf-why-me">Studio</a></li>
                        <li><a href="#tf-about">Rekaman</a></li>
                        <li><a href="#tf-portfolio">Alat</a></li>
                        <li><a href="#tf-contact">Tentang Kami</a></li>
                      </ul>
                    </div>
                </div>
            </nav>

            <div class="container">
                <div class="content">
                    <h1>SELAMAT DATANG</h1>
                    <a href="index.html"><img src="img/mk coret.png" width="150" align="center"></a>
                    <h2>MK Music Studio</h2>
                    <h3>Solution For Your Music Studio Needs</h3>
                    <br>
                    <a href="login.php" class="btn btn-primary my-btn">Login Untuk Booking</a>
                </div>
            </div>
        </div>
    </div>

    <div id="tf-why-me">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 align="center">Sewa Studio Musik</h2>
                        <br>
                        <p align="center">Solusi tepat bagi kebutuhan band anda yang membutuhkan tempat penyewaan studio. Studio musik yang dilengkapi dengan full AC dan alat-alat musik dengan kualitas tinggi sehingga dapat memberikan pengalaman bermusik yang nyaman bagi anda.</p>
                        <br>
                        <p>Hanya dengan biaya sewa Rp. 40.000,- per jam anda dapat merasakan sensasi bermusik yang nyaman. Login untuk booking secara online dengan DP 50% yang dapat dibayarkan melalui transfer ke rekening MK Music Studio (Sertakan foto bukti transfer).</p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tf-about">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6">
                        <h2 align="center">Studio Rekaman</h2>
                        <br>
                        <p align="center">Solusi tepat bagi kebutuhan band anda yang membutuhkan tempat rekaman untuk merekam karya lagu anda. Tempat rekaman kami memberikan fasilitas alat rekaman yang berkualitas dan juga tenaga yang berpengalaman, sehingga kami menjamin kepuasan anda dengan hasil rekaman karya lagu yang berkualitas. </p>
                        <br>
                        <p align="center">Hanya dengan biaya Rp. 650.000,- per lagu anda sudah mendapatkan paket Recording, Mixing dan Mastering. Studio kami juga menyediakan jasa Recording, Mixing dan Mastering secara terpisah dengan harga masing-masing Rp. 250.000,- tiap jasa yang dipilih. Pembayaran dapat dilakukan dengan transfer ke rekening MK Studio Music dengan DP minimal 50% (Sertakan foto bukti transfer).</p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tf-portfolio">
        <div class="container">
            <div class="section-title">
                <h2>Equipment Studio Kami</h2>
                <hr>
            </div>

            <div class="space"></div>

            <div class="row" style="border: 0; padding: 20px; background: none; width: 1000px; text-align: center;">
                <table style="width: 110%;">
                    <tr>
                        <td>
                            <img alt="Recorders" src="img/recorders2.png">
                        </td>
                        <td>
                            <img alt="Mixers" src="img/mixers2.png">
                        </td>
                        <td>
                            <img alt="Audio Interface" src="img/outboard2.png">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 class="media-heading">Recorders</h3>
                            <p>- Presonus Studio One 3 (DAW)</p>
                        </td>
                        <td>
                            <h3 class="media-heading">Mixers</h3>
                            <p>- Behringer XENYX X1622 USB Audio Mixer</p>
                        </td>
                        <td>
                            <h3 class="media-heading">Microphones</h3>
                            <p>- Shure SM58 (Vocal)</p>
                            <p>- Shure SM57 (Drum)</p>
                        </td>
                    </tr>
                </table>

                <br>

                <table style="width: 110%;">
                    <tr>
                        <td>
                            <img alt="Audio Interface" src="img/outboard2.png">
                        </td>
                        <td>
                            <img alt="Monitoring" src="img/monitoring2.png">
                        </td>
                        <td>
                            <img alt="Instruments" src="img/instruments2.png">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3 class="media-heading">Audio Interface</h3>
                            <p>- Focusrite 2i2 2nd Generation Audio Interface</p>
                        </td>
                        <td>
                            <h3 class="media-heading">Monitoring</h3>
                            <p>- KRK Rokit 5 G3 (Sound Monitor)</p>
                            <p>- Behringer HPS5000 (Headphone)</p>
                        </td>
                        <td>
                            <h3 class="media-heading">Instruments</h3>
                            <p>- ESP Gitar Elektrik LTD EC100QM</p>
                            <p>- Gitar IBANEZ RG Series RG250-BK – Black</p>
                            <p>- IBANEZ Bass SR Series [SR300E-IPT]</p>
                            <p>- Drum TAMA IP52KH6</p>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
    </div>

    <div id="tf-contact">
        <div class="container">
            <div class="section-title" style="color: #FFFFFF">
                <h2>Tentang Kami</h2>
            </div>

            <div class="space"></div>

            <div class="row" align="center">
                <div style="border: 0; padding: 20px; background: url(img/transparan.png); width: 800px; text-align: left; color: #FFFFFF;">
                        <table width=100% rules="cols" cellspacing=40>
                            <tr>
                                <td width=25%>
                                    <p>Jalan Halmahera 1 No. 24</p>
                                    <p>Sumbersari, Jember</p>
                                    <p>MKMusic@gmail.com</p>
                                    <p>Tel : 081230497824</p>
                                    <p>WA : 081230497824</p>
                                </td>
                                <td width=50% style="text-align: center;">
                                    <p>Jadwal Kerja Studio Kami</p>
                                    <p>----------------------------------------------</p>
                                    <p>Senin - Jumat : 8am - 11pm</p>
                                    <p>Sabtu : 10am - 11pm</p>
                                    <p>Minggu : 12pm - 11pm</p>                                    
                                </td>
                                 <td width=25% align="center">
                                    <img src="img/mk coret.png" style="width: 150px">                           
                                </td>
                            </tr>
                        </table>
                        
                        <hr>
                        
                        <p>Kunjungi Juga Situs Kami : </p>
                        
                        <table width=100%>
                            <tr>
                                <td><img src="img/logofb.png">: MKMusicStudio </td>
                                <td><img src="img/logoig.png">: @MKMusicStudio </td>
                                <td><img src="img/logoyoutube.png">: MKMusicStudio </td>
                                <td><img src="img/logosc.png">: MKMusicStudio </td>
                            </tr>
                        </table>

                </div>
            </div>
        </div>
    </div>

    <nav id="tf-footer">
        <div class="container">
             <div class="pull-left">
                <p>2017 © Official Website MK Music Studio. All Rights Reserved. Designed and Coded by MK Music Management for <a href="https://themeforces.com">MK Music Studio</a></p>
            </div>
        </div>
    </nav>
   

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>