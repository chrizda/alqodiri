<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/18
 * Time: 10:55 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{

    function login($u,$p){
        $this->db->from('admin');
        $this->db->where('username',$u);
        $this->db->where('password',md5($p));
        $a = $this->db->get();
        if($a->num_rows() == 1){
            $data = $a->result_array();
            $this->session->set_userdata('username', $data[0]['username']);
            $this->session->set_userdata('level', $data[0]['level']);
            return true;
        }
        else{
            return false;
        }
    }

    function register($datauser, $datapelanggan){
        $this->db->insert('admin',$datauser);
        $this->db->insert('pelanggan',$datapelanggan);
    }

}