<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/18
 * Time: 10:55 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model{

    function pilihproduk(){
        return $this->db->get('produk');
    }

        function tambahpesanan($data, $table){
        $this->db->insert($table, $data);
    }

    function pelanggan($where, $table){
        return $this->db->get_where($table, $where);
    }

    function updatepelanggan($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function ambilidpelanggan($username){
        $this->db->select('id_pelanggan');
        $this->db->where('id_user',$username);
        return $this->db->get('pelanggan');
    }

    function pesanan($where, $table){
        return $this->db->get_where($table, $where);
    }

    function statuspesanan($id_pelanggan){
        $this->db->from('pesanan');
        $this->db->where('status','Belum diproses');
        $this->db->where('id_pelanggan',$id_pelanggan);
        return $this->db->get()->num_rows();
    }

    function updatepesanan($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function hapuspesanan($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function pesanproduk($where, $table){
        return $this->db->get_where($table, $where);
    }

    function detailpesanan($where){
        $this->db->select("detail_pesanan.*, produk.*, sum(jumlah) as sum_jumlah");
        $this->db->from('detail_pesanan');
        $this->db->join('produk', 'produk.id_produk = detail_pesanan.id_produk', 'inner');
        $this->db->where($where);
        $this->db->group_by('nama_produk');
        $data = $this->db->get();
        return $data;
    }

    function ambilidpesanan($where, $table){
        return $this->db->get_where($table, $where);
    }

    function tambahdetail($data, $table){
        $this->db->insert($table, $data);
    }

    function uploadnota(){
        $config['upload_path'] = './assets/nota/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']  = '2048';
        $config['remove_space'] = TRUE;

        $this->load->library('upload', $config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    // Fungsi untuk menyimpan data ke database
    public function save($upload){
        $id_pesanan = $this->input->post('id_pesanan');
        $tanggal = $this->input->post('tanggal');
        $total = $this->input->post('total');
        $status = $this->input->post('status');
        $id_pelanggan = $this->input->post('id_pelanggan');
        $data = array(
            'tanggal' => $tanggal,
            'total' => $total,
            'status' => $status,
            'bukti_bayar' => $upload['file']['file_name'],
            'id_pelanggan' => $id_pelanggan
        );
        $where = array(
            'id_pesanan' => $id_pesanan
        );

        $this->db->update('pesanan', $data, $where);
    }

}