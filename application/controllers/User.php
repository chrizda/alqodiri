<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/1/18
 * Time: 11:34 AM
 */
class User extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('M_user');
        $this->load->library('cart');
    }

    function index(){
        $this->load->view('User/header');
        $this->load->view('User/index');
        $this->load->view('User/footer');
    }

    function carapembayaran(){
        $this->load->view('User/header');
        $this->load->view('User/carapembayaran');
        $this->load->view('User/footer');
    }

    function sejarah(){
        $this->load->view('User/header');
        $this->load->view('User/sejarah');
        $this->load->view('User/footer');
    }

    function editpelanggan(){
        $username = $this->session->userdata('username');
        $id_pelanggan = $this->M_user->ambilidpelanggan($username)->row()->id_pelanggan;
        $where = array('id_pelanggan' => $id_pelanggan);
        $data['pelanggan'] = $this->M_user->pelanggan($where, 'pelanggan')->result();
        $this->load->view('User/header');
        $this->load->view('User/editpelanggan', $data);
        $this->load->view('User/footer');
    }

    function updatepelanggan(){
        $id_pelanggan = $this->input->post('id_pelanggan');
        $id_user = $this->input->post('id_user');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $no_hp = $this->input->post('no_hp');

        $data = array(
            'id_user' => $id_user,
            'nama' => $nama,
            'alamat' => $alamat,
            'jenis_kelamin' => $jenis_kelamin,
            'no_hp' => $no_hp
        );

        $where = array(
            'id_pelanggan' => $id_pelanggan
        );

        $this->M_user->updatepelanggan($where,$data,'pelanggan');
        redirect('User/index');
    }

    function katalogproduk(){
        $username = $this->session->userdata('username');
        $id_pelanggan = $this->M_user->ambilidpelanggan($username)->row()->id_pelanggan;
        $data['statuspesanan'] = $this->M_user->statuspesanan($id_pelanggan);
        $data['produk'] = $this->M_user->pilihproduk()->result();
        $this->load->view('User/header');
        $this->load->view('User/katalogproduk', $data);
        $this->load->view('User/footer');
    }

    function pilihproduk(){
        $data['produk'] = $this->M_user->pilihproduk()->result();
        $this->load->view('User/header');
        $this->load->view('User/pilihproduk', $data);
        $this->load->view('User/footer');
    }

    function tambahpesanan(){
        $username = $this->session->userdata('username');
        $tanggal = date('Y-m-d');
        $status = 'Belum diproses';
        $id_pelanggan = $this->M_user->ambilidpelanggan($username)->row()->id_pelanggan;

        $data = array(
            'tanggal' => $tanggal,
            'total' => null,
            'bukti_bayar' => null,
            'status' => $status,
            'id_pelanggan' => $id_pelanggan
        );
        $this->M_user->tambahpesanan($data, 'pesanan');
        redirect('User/pesanan');
    }

    function updatepesanan(){
        $id_pesanan = $this->input->post('id_pesanan');
        $tanggal = $this->input->post('tanggal');
        $total = $this->input->post('total');
        $bukti_bayar = $this->input->post('bukti_bayar');
        $status = $this->input->post('status');
        $id_pelanggan = $this->input->post('id_pelanggan');

        $data = array(
            'tanggal' => $tanggal,
            'total' => $total,
            'bukti_bayar' => $bukti_bayar,
            'status' => $status,
            'id_pelanggan' => $id_pelanggan
        );

        $where = array(
            'id_pesanan' => $id_pesanan
        );

        $this->M_user->updatepesanan($where,$data,'pesanan');
        redirect('User/pesanan');
    }

    function pesanan(){
        $username = $this->session->userdata('username');
        $id_pelanggan = $this->M_user->ambilidpelanggan($username)->row()->id_pelanggan;
        $where = array('id_pelanggan' => $id_pelanggan);
        $data['pesanan'] = $this->M_user->pesanan($where, 'pesanan')->result();
        $this->load->view('User/header');
        $this->load->view('User/pesanan', $data);
        $this->load->view('User/footer');
    }

    function detailpesanan($id){
        $where = array('id_pesanan' => $id);
        $data['detailpesanan'] = $this->M_user->detailpesanan($where)->result();
        $data['idpesanan'] = $this->M_user->ambilidpesanan($where, 'pesanan')->result();
        $this->load->view('User/header');
        $this->load->view('User/detailpesanan', $data);
        $this->load->view('User/footer');
    }

    function buatpesanan($id_pesanan){
        $where = array('id_pesanan' => $id_pesanan);
        $data['pesanan'] = $this->M_user->ambilidpesanan($where, 'pesanan')->row();
        $data['produk'] = $this->M_user->pilihproduk()->result();
        $this->load->view('User/header');
        $this->load->view('User/buatpesanan', $data);
        $this->load->view('User/footer');
    }

    function hapuspesanan($id){
        $where = array('id_pesanan' => $id);
        $this->M_user->hapuspesanan($where, 'pesanan');
        redirect('User/katalogproduk');
    }

    function tambahdetail($id){
        $id_pesanan = $this->input->post('id_pesanan');
        $id_produk = $this->input->post('id_produk');
        $jumlah = $this->input->post('jumlah');

        $data = array(
            'id_pesanan' => $id_pesanan,
            'id_produk' => $id_produk,
            'jumlah' => $jumlah
        );
        $this->M_user->tambahdetail($data, 'detail_pesanan');
        redirect('User/detailpesanan/'.$id);
    }

    function tambahnota($id_pesanan){
        $where = array('id_pesanan' => $id_pesanan);
        $data['pesanan'] = $this->M_user->pesanan($where, 'pesanan')->result();
        $this->load->view('User/header');
        $this->load->view('User/tambahnota', $data);
        $this->load->view('User/footer');
    }

    function uploadnota(){
        $data = array();
        if($this->input->post()){ // Jika user menekan tombol Submit (Simpan) pada form
            // lakukan upload file dengan memanggil function upload yang ada di M_user.php
            $upload = $this->M_user->uploadnota();

            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Panggil function save yang ada di M_user.php untuk menyimpan data ke database
                $this->M_user->save($upload);

                redirect('User/pesanan'); // Redirect kembali ke halaman awal / halaman view data
            }else{ // Jika proses upload gagal
                $data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
        $this->load->view('User/tambahnota', $data);
    }
}
?>