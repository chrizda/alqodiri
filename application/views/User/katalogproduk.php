<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:15 PM
 */
?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Katalog Produk</h2>
            <h3>Pilihan produk yang bisa anda pesan.</h3>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <table>
                <tr>
                    <?php foreach ($produk as $p) { ?>
                    <td style="width: 10px;"></td>
                    <td>
                        <div style="padding: 20px; background: url(<?=base_url()?>assets/vendors/img/transparan.png); color: #FFFFFF;">
                            <table>
                                <tr>
                                    <td width=300px; align="center">
                                        <img src="<?=base_url()?>assets/vendors/img/<?php echo $p->gambar ?>" style="width: 150px">
                                        <h3><?php echo $p->nama_produk ?></h3>
                                        <p><?php echo $p->keterangan ?></p>
                                        <p>Harga <?php echo "Rp. ".number_format($p->harga,2,',','.'); ?> per <?php echo $p->satuan ?>.</p>
                                        <p>Sisa Stok : <?php echo $p->stok ?> <?php echo $p->satuan ?>.</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 10px;"></td>
                    <?php } ?>
                </tr>
            </table>

            <br>
            <?php if (empty($statuspesanan) or $statuspesanan<1) { ?>
                <table >
                    <tr>
                        <form action="<?php echo base_url().'User/tambahpesanan';?>" method="post">
                            <input type="hidden" name="tanggal">
                            <input type="hidden" name="total">
                            <input type="hidden" name="bukti_bayar">
                            <input type="hidden" name="status">
                            <input type="hidden" name="id_pelanggan">
                            <td><button type="submit" class="btn btn-primary my-btn">Buat Pesanan</button></td>
                        </form>
                        <td><a href="<?php echo site_url('User/index') ?>" class="btn btn-primary my-btn">Kembali</a></td>
                    </tr>
                </table>
            <?php }else { ?>
                <table >
                    <tr>
                        <td><a href="<?php echo site_url('User/pesanan') ?>" class="btn btn-primary my-btn">Lihat Pesanan</a></td>
                        <td><a href="<?php echo site_url('User/index') ?>" class="btn btn-primary my-btn">Kembali</a></td>
                    </tr>
                </table>
            <?php } ?>
        </div>

    </div>
</div>