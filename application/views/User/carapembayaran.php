<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/24/18
 * Time: 1:51 PM
 */
?>

<div id="tf-contact">
    <div class="content">
        <h1>CARA PEMBAYARAN</h1>
        <img src="<?=base_url()?>assets/vendors/img/logoQ.png" width="150" align="center">
        <br>
        <h4>Pemesanan dilakukan dengan cara :<br>
            1. Buat pesanan dengan cara klik katalog produk lalu klik tombol buat pesanan <br>
            2. Masukkan barang apa saja yang ingin dibeli <br>
            3. Transfer ke nomor rekening 000501001641300 sesuai dengan total harga yang tertera <br>
            4. Upload bukti nota pembayaran di tabel pemesanan yang sudah dibuat <br>
            5. Tunggu validasi dari kami dan kami akan langsung mengirim barang pesanan
            </h4>
        <a href="<?=base_url('User')?>" class="btn btn-primary my-btn">Kembali</a>
    </div>
</div>
