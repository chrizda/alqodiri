<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 4/20/2018
 * Time: 4:15 PM
 */
?>

<div id="tf-contact">
    <div class="content">
        <h1>SELAMAT DATANG</h1>
        <img src="<?=base_url()?>assets/vendors/img/logoQ.png" width="150" align="center">
        <h2>Website Pemesanan Air Mineral Al Qodiri</h2>
        <h3>Pemesanan Online Mudah dan Cepat</h3>
        <br>
        <a href="<?=base_url('User/sejarah')?>" class="btn btn-primary my-btn">Tentang Al Qodiri</a>
        <a href="<?=site_url('User/katalogproduk')?>" class="btn btn-primary my-btn">Katalog Produk</a>
        <a href="<?=base_url('User/carapembayaran')?>" class="btn btn-primary my-btn">Cara Pemesanan</a>
    </div>
</div>
