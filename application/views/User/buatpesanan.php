<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 4/21/2018
 * Time: 6:17 PM
 */ ?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Tambah Pesanan Produk</h2>
            <h3>Silahkan pilih produk yang ingin anda pesan.</h3>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <form method="post" action="<?php echo base_url().'User/tambahdetail/'.$pesanan->id_pesanan;?>">
                <table>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Id Pesanan</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $pesanan->id_pesanan ?>" name="id_pesanan" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Jenis Produk</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <select style="width: 100%" type="text" class="form-group" name="id_produk" required>
                                <option>Pilih Jenis Produk</option>
                                <?php foreach ($produk as $p) { ?>
                                    <option value="<?php echo $p->id_produk ?>"><?php echo $p->nama_produk ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Jumlah Pesanan</label></td>
                        <td width="5%"style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td><input style="width: 100%" type="text" class="form-group" placeholder="Masukkan Jumlah Pesanan Anda" name="jumlah" required></td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary my-btn">Kirim</button>
                <a href="" class="btn btn-primary my-btn">Batal</a>
             </form>
        </div>
    </div>
</div>