<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 4/22/2018
 * Time: 6:23 PM
 */ ?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Daftar Pesanan Anda</h2>
            <h3>Klik detail pesanan untuk memesan produk. Jika sudah silahkan kirim bukti pembayaran anda.</h3>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <table class="table table-striped table-bordered" style="width: 900px">
                <thead>
                <tr>
                    <th width="5%">Id</th>
                    <th>Tanggal</th>
                    <th>Total Bayar</th>
                    <th width="5%">Nota</th>
                    <th>Status</th>
                    <th width="15%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pesanan as $a) { ?>
                    <tr style="text-align: center;">
                        <td><?php echo $a->id_pesanan ?></td>
                        <td><?php echo $a->tanggal ?></td>
                        <td><?php echo "Rp. ".number_format($a->total,2,',','.'); ?></td>
                        <td>
                            <?php if (!empty($a->bukti_bayar)){ ?>
                                <a class="btn btn-sm btn-success" href="<?=site_url('User/tambahnota/'.$a->id_pesanan) ?>"><li class="fa fa-check"> Lihat Nota</li></a>
                            <?php } else{ ?>
                                <a class="btn btn-sm btn-primary" href="<?=site_url('User/tambahnota/'.$a->id_pesanan) ?>"><li class="fa fa-plus"> Tambah Nota</li></a>
                            <?php } ?>
                        </td>
                        <td><?php echo $a->status ?></td>
                        <td>
                            <?php if (!empty($a->total)){ ?>
                                <a class="btn btn-sm btn-primary" href="<?=site_url('User/detailpesanan/'.$a->id_pesanan) ?>"><li class="fa fa-eye"></li></a>
                            <?php } else { ?>
                                <a class="btn btn-sm btn-primary" href="<?=site_url('User/buatpesanan/'.$a->id_pesanan);?>"><li class="fa fa-plus"></li></a>
                            <?php } ?>
                            <?php if ($a->status == 'Belum diproses'){ ?>
                            <a data-confirm="Apakah anda yakin membatalkan pesanan ini?" class="batal btn btn-sm btn-danger" href="<?=site_url('User/hapuspesanan/'.$a->id_pesanan) ?>"><li class="fa fa-remove"></li></a>
                            <?php }?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>