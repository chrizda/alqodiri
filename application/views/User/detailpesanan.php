<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 4/22/2018
 * Time: 6:23 PM
 */ ?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Detail Pesanan</h2>
            <h3>Klik Tambah Produk untuk menambah pesanan produk. Jika sudah silahkan klik Simpan.</h3>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Jumlah Pesanan</th>
                    <th>Sub Total</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($detailpesanan as $a) { ?>
                    <tr>
                        <td><?php echo $a->nama_produk ?></td>
                        <td><?php echo "Rp. ".number_format($a->harga,2,',','.'); ?></td>
                        <td><?php echo $a->sum_jumlah ?> buah</td>
                        <td><?php $total=0; $sub[] = $subtotal = intval($a->harga) * intval($a->sum_jumlah); echo "Rp. ".number_format($subtotal,2,',','.'); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total Harga</td>
                        <td><?php $total = $total + array_sum($sub); echo "Rp. ".number_format($total,2,',','.');?></td>
                    </tr>
                </tbody>
            </table>
            <?php foreach ($idpesanan as $dp) { ?>
                <table>
                    <tr>
                        <td>
                            <form action="<?php echo base_url().'User/updatepesanan';?>" method="post">
                                <input type="hidden" value="<?php echo $dp->id_pesanan ?>" name="id_pesanan" readonly>
                                <input type="hidden" value="<?php echo $dp->tanggal ?>" name="tanggal" readonly>
                                <input type="hidden" value="<?php echo $total ?>" name="total" readonly>
                                <input type="hidden" value="<?php echo $dp->bukti_bayar ?>" name="bukti_bayar" readonly>
                                <input type="hidden" value="<?php echo $dp->status ?>" name="status" readonly>
                                <input type="hidden" value="<?php echo $dp->id_pelanggan ?>" name="id_pelanggan" readonly>
                                <button type="submit" class="btn btn-primary my-btn">Simpan</button>
                            </form>
                        </td>
                        <td>
                            <a class="btn btn-primary my-btn" href="<?=site_url('User/buatpesanan/'.$dp->id_pesanan);?>">Tambah Produk</a>
                        </td>
                    </tr>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
