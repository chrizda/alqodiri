<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/24/18
 * Time: 1:37 PM
 */
?>

<div id="tf-contact">
    <div class="content">
        <h1>TENTANG AL QODIRI</h1>
        <img src="<?=base_url()?>assets/vendors/img/logoQ.png" width="150" align="center">
        <br>
        <div style="margin-left:20px;margin-right:20px;">
        <h4 style="text-align:justify;color:#000">
            Beroperasi sejak tahun 2012, Air Minum Dalam Kemasan Al Qodiri (Air Al Qodiri) terus berkembang dari yang semula hanya ditujukan untuk pemenuhan kebutuhan air minum Jamaah Pengajian Manakib di Pondok Pesantren Al Qodiri menjadi jauh lebih luas dengan melayani konsumen umum. Dibawah pengelolaan Manajemen CV Seven Dream Air Al Qodiri dalam kurun waktu 5 tahun ini terus mengalami pengembangan. Inovasi dan kualitas produk yang menjadi fokus utama membuat produk kami diterima masyarakat Jember danbahkan saat ini telah mencapai Karasidenan Besuki dan sekitarnya. Menjawab hal itu, manajemen terus berusaha meningkatkan kapasitas produksi dan layanan kepada pelanggan dengan salah satunya peningkatan status badan usaha menjadi PT Tujuh Impian Bersama sejak tahun 2015. Keseriusan ini kemudian kami lanjutkan dalam rangka meningkatkan mutu produk dan layanan kepada pelanggan dengan diperolehnya Sertifikat Manajemen Mutu ISO 9001:2008 pada tahun 2016.
        </h4>
        </div>
        <a href="<?=base_url('User')?>" class="btn btn-primary my-btn">Kembali</a>
    </div>
</div>
