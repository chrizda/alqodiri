<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:01 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?=base_url('assets/vendors/img/logoQ.png')?>" type="image/jpg" />

    <title>Al Qodiri</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="<?=base_url();?>/assets/vendors/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/font-awesome/css/font-awesome.css">
    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="<?=base_url();?>/assets/vendors/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/css/responsive.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url();?>/assets/vendors/js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="tf-home">
    <div style="background: #222222;">
         <div id="sticky-anchor"></div>
        <nav id="tf-menu" class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" style="height: 10px;">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo site_url('User/index')?>">Beranda</a></li>
                        <li><a href="<?php echo site_url('User/katalogproduk')?>">Katalog</a></li>
                        <li><a href="<?php echo site_url('User/pesanan')?>">Pesanan</a></li>
                    </ul>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo site_url('User/editpelanggan')?>">Akun</a></li>
                        <li><a href="<?=base_url('Login/logout')?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
