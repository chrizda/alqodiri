<?php
/**
 * Created by PhpStorm.
 * User: Akbarrul Mahrifat
 * Date: 5/2/2018
 * Time: 12:00 AM
 */ ?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Upload Bukti Pembayaran</h2>
            <h3>Silahkan upload bukti transfer dari pesanan anda.</h3>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <!-- Menampilkan Error jika validasi tidak valid -->
            <div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
            <?php echo form_open("User/uploadnota", array('enctype'=>'multipart/form-data')); ?>
            <?php foreach ($pesanan as $a) { ?>
                <input type="hidden" value="<?php echo $a->id_pesanan; ?>" name="id_pesanan">
                <input type="hidden" value="<?php echo $a->tanggal; ?>" name="tanggal">
                <input type="hidden" value="<?php echo $a->total; ?>" name="total">
                <input type="hidden" value="<?php echo $a->status; ?>" name="status">
                <input type="hidden" value="<?php echo $a->id_pelanggan; ?>" name="id_pelanggan">
                <?php if (!empty($a->bukti_bayar)) { ?>
                    <div style=" width: 200px; padding: 20px; background: url(<?=base_url()?>assets/vendors/img/transparan.png); color: #FFFFFF;">
                        <img src="<?=base_url()?>assets/nota/<?php echo $a->bukti_bayar;?>" width="150">
                        <hr>
                        <p><?php echo $a->bukti_bayar; ?></p>
                    </div>
                    <a href="<?=site_url('User/pesanan'); ?>" class="btn btn-primary my-btn">Kembali</a>
                <?php }else{ ?>
                    <table cellpadding="200">
                        <tr>
                            <td>Upload Nota Pesanan Anda Disini : </td>
                            <td width="5%"></td>
                            <td><input type="file" name="gambar"></td>
                        </tr>
                    </table>
                    <hr>
                    <button class="btn btn-primary my-btn" type="submit" name="submit">Simpan</button>
                    <a href="<?=site_url('User/pesanan'); ?>" class="btn btn-primary my-btn">Batal</a>
                <?php } ?>
            <?php } ?>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>