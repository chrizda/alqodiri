<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:13 PM
 */
?>

<nav id="tf-footer">
    <div class="container">
        <div class="pull-left">
            <p></p>
        </div>
    </div>
</nav>
<!-- /footer content -->

<!-- jQuery -->
<script src="<?=base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url();?>assets/vendors/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?=base_url();?>assets/vendors/nprogress/nprogress.js"></script>

<script src="<?=base_url();?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?=base_url()?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?=base_url();?>assets/vendors/build/js/custom.min.js"></script>

<script>
    $('.validasi').on("click", function (e) {
        e.preventDefault();

        var choice = confirm($(this).attr('data-confirm'));

        if (choice) {
            window.location.href = $(this).attr('href');
        }
    });

    $(document).ready(function() {
        $('#validasi').DataTable();
    } );
</script>

</body>
</html>

